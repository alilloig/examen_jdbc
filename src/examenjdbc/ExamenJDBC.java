/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examenjdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 *
 * @author Álvaro Lillo Igualada <@alilloig>
 */
public class ExamenJDBC {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args){
    Scanner input = new Scanner(System.in);
    Connection con;
    Statement query;
    ResultSet result;
    try {
      con = DriverManager.getConnection(
              "jdbc:mysql://localhost/employees?serverTimezone=UTC","alilloig","sql");
      System.out.println("Conexion establecida");
      query = con.createStatement();
      System.out.print("Programa que muestra los empleados no jefes que cobran mas de"
              + " cierta cantidad. Por favor, introduzca dicha cantidad: ");
      result = query.executeQuery("SELECT first_name, last_name, salary FROM employees "
              + "LEFT JOIN dept_manager ON employees.emp_no = dept_manager.emp_no "
              + "INNER JOIN salaries ON employees.emp_no = salaries.emp_no WHERE "
              + "dept_manager.emp_no IS NULL AND salary >"+input.next()+" LIMIT 1000;");
      while (result.next()){
        System.out.print("Nombre: "+result.getString("first_name"));
        System.out.print(", Apellido: "+result.getString("last_name"));
        System.out.println(", Salario: "+result.getString("salary"));
      }
      result.close();
      query.close();
      con.close();
    } catch (SQLException ex) {
      System.out.println("Error en la conexion con employees: "+ex.toString());
    }
  }
}
